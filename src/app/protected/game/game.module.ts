import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRoutingModule } from './game-routing.module';
import { GameComponent } from './components/game/game.component';
import { GameMenuComponent } from './components/game-menu/game-menu.component';
import { GameResourcesComponent } from './components/game-resources/game-resources.component';
import { GameSummaryComponent } from './components/game-summary/game-summary.component';

@NgModule({
  imports: [
    CommonModule,
    GameRoutingModule
  ],
  declarations: [
    GameComponent,
    GameMenuComponent,
    GameResourcesComponent,
    GameSummaryComponent
  ]
})
export class GameModule { }
