import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() {
    // this.languageChange = new EventEmitter();
  }

  ngOnInit() {
  }

  languageChanged(language: string) {
    console.log(`Nav-bar: He recibido el idioma: ${language}.`);
  }

}
