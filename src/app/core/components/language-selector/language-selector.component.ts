import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.css']
})
export class LanguageSelectorComponent implements OnInit {
  selectedValue: string;
  flagicon: string;
  availableLangs: string[];

  constructor(private tranlateService: TranslateService) {
    this.availableLangs = this.tranlateService.getLangs();
    this.changeFlag();
  }

  ngOnInit() {
    this.selectedValue = 'es';
  }

  onLanguageChanged(language: string) {
    this.tranlateService.use(language);
    this.changeFlag();
  }

  changeFlag() {
    this.tranlateService.get('FLAG_ICON').subscribe(
      x => {
        this.flagicon = x;
      }
    );
  }
}
